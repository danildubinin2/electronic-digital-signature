package com.dubinin.esd.finaltry;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.FileInputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Base64;

/**
 * @author daniel
 */
public class Main {
    private static String text = "{'field1': 'value', 'field2': value}";

    public static void main(String[] args) throws Exception {
        String password = "123";
        Signature messageSignature = new Signature();
        messageSignature.createKeyStore(password);

        FileInputStream fileInputStreamSecur = new FileInputStream("/home/daniel/Java/esd/ks.jks");

        KeyPair pair = messageSignature.getPair(fileInputStreamSecur, "ksalias", "arvinser".toCharArray(), password.toCharArray());
        PrivateKey aPrivate = pair.getPrivate();
        PublicKey aPublic = pair.getPublic();

        byte[] signature = Signature.signData(text.getBytes(), aPrivate);
        System.out.println("text bytes: " + Arrays.toString(text.getBytes()));
        System.out.println("signature bytes: " + Arrays.toString(signature));
        boolean verifyResult = Signature.verifySig(text.getBytes(), aPublic, signature);

        System.out.println(verifyResult);

        //encrypting using public key from file---------------------------------------------
        Cipher cipher = Cipher.getInstance("RSA");

        byte[] toEncrypt = text.getBytes();
        try {
            cipher.init(Cipher.ENCRYPT_MODE, aPublic);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        byte[] rawEncrypted = {};
        try {
            rawEncrypted = cipher.doFinal(toEncrypt);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }

        String encodedString = Base64.getEncoder().encodeToString(rawEncrypted);
        System.out.println("encoded: " + encodedString);

        //decrypting using private key from file---------------------------------
        byte[] rawEncryptedToDecript = Base64.getDecoder().decode(encodedString);

        try {
            cipher.init(Cipher.DECRYPT_MODE, aPrivate);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        byte[] rawDecrypted = {};
        try {
            rawDecrypted = cipher.doFinal(rawEncryptedToDecript);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }

        String decrypted = new String(rawDecrypted);
        System.out.println("decrypted: " + decrypted);
    }
}
