package com.dubinin.esd.finaltry;

import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * @author daniel
 */
public class Signature {

    public Signature() {

    }

    public void createKeyStore(String signaturePasword) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, NoSuchProviderException, SignatureException, InvalidKeyException {
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] password = "arvinser".toCharArray();
        ks.load(null, password);

        FileOutputStream fos = new FileOutputStream("/home/daniel/Java/esd/ks.jks");
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
        keyGen.initialize(1024, random);
        KeyPair pair = keyGen.generateKeyPair();
        String domainName = "example.org";
        {
            Security.addProvider(new BouncyCastleProvider());
        }
        X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();

        int serial = new SecureRandom().nextInt();

        v3CertGen.setSerialNumber(BigInteger.valueOf(serial < 0 ? -1 * serial : serial));
        v3CertGen.setIssuerDN(new X509Principal("CN=" + domainName + ", OU=None, O=None L=None, C=None"));
        v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 30));
        v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 365 * 10)));
        v3CertGen.setSubjectDN(new X509Principal("CN=" + domainName + ", OU=None, O=None L=None, C=None"));

        v3CertGen.setPublicKey(pair.getPublic());
        v3CertGen.setSignatureAlgorithm("MD5WithRSAEncryption");
        X509Certificate PKCertificate = v3CertGen.generateX509Certificate(pair.getPrivate());

        KeyStore.PrivateKeyEntry entry = new KeyStore.PrivateKeyEntry(pair.getPrivate(),
                new java.security.cert.Certificate[]{PKCertificate});
        ks.setEntry("ksalias", entry, new KeyStore.PasswordProtection(signaturePasword.toCharArray()));
        ks.store(fos, password);

        fos.close();
    }

    public KeyPair getPair(FileInputStream in, String alias, char[] passKeyStore, char[] passAlias)
            throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException,
            UnrecoverableEntryException {
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(in, passKeyStore);
        Key key = ks.getKey(alias, passAlias);
        if (key instanceof PrivateKey) {
            // Get certificate of public key
            java.security.cert.Certificate cert = ks.getCertificate(alias);
            PublicKey publicKey = cert.getPublicKey();
            return new KeyPair(publicKey, (PrivateKey) key);
        }
        return null;
    }

    public static byte[] signData(byte[] data, PrivateKey key) throws Exception {
        java.security.Signature signer = java.security.Signature.getInstance("SHA1withRSA");
        signer.initSign(key);
        signer.update(data);
        return (signer.sign());
    }

    public static boolean verifySig(byte[] data, PublicKey key, byte[] sig) throws Exception {
        java.security.Signature signer = java.security.Signature.getInstance("SHA1withRSA");
        signer.initVerify(key);
        signer.update(data);
        return (signer.verify(sig));

    }


}
